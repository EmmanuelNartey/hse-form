package com.example.android.hsechecklist;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GeneralActivity tab1 = new GeneralActivity();
                return tab1;
            case 1:
                AwarenessActivity tab2 = new AwarenessActivity();
                return tab2;
            case 2:
                AccomodationActivity tab3 = new AccomodationActivity();
                return tab3;
            case 3:
                RecreationActivity tab4 = new RecreationActivity();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}